#!/usr/bin/python
#
# This script can calculate any single attribute for a 555 timer circuit
# in astable or monostable configuration.
#
# USAGE:
#
# ./555Timer.py -a [PARAMETERS]...
# ./555Timer.py -m [PARAMETERS]...
#
# EXAMPLES:
#    Solve for 'c' in astable setup, with given values:
#    ./555Timer.py -a -r1 1000 -r2 100000 -f 10.0
#
#    Solve for pulse length in monostable setup:
#    ./555Timer.py -m -r 1000 -c 0.0000047
#
# PARAMETERS:
#    Astable (must supply any 3):
#    -r1 Value of R1 in Ohms
#    -r2 Value of R2 in Ohms
#    -c  Value of C in Farads (float)
#    -f  Output frequency in Hz (float)
#
#    Monostable (must supply any 2):
#    -r Value of R in Ohms
#    -c Value of C in Farads (float)
#    -t Length of output pulse in seconds (float)
#

import math
import argparse

# Parse arguments
parser = argparse.ArgumentParser(description='Calculate 555 timer circuit parameters')

group = parser.add_mutually_exclusive_group()
group.add_argument("-a", "--astable",    action="store_true", help="Perform calculation for an astable setup")
group.add_argument("-m", "--monostable", action="store_true", help="Perform calculation for a monostable setup")

parser.add_argument("-r1", type=int,   help="Value for R1 in Ohms (astable)")
parser.add_argument("-r2", type=int,   help="Value for R2 in Ohms (astable)")
parser.add_argument("-f",  type=float, help="Output frequency in Hertz (astable)")
parser.add_argument("-c",  type=float, help="Value for C in Farads (either)")
parser.add_argument("-r",  type=int,   help="Value for R in Ohms   (monostable)")
parser.add_argument("-t",  type=float, help="Length of output pulse in seconds (monostable)")

args = parser.parse_args()

error_msg = "Improper arguments. Type './555Timer.py -h' for more info"

if args.astable:
    if args.f == None:
	try:
            print "f: %1.2e Hz" % (1 / (math.log(2) * args.c * (args.r1 + 2 * args.r2)))
	except:
            print error_msg

    elif args.c == None:
	try:
            print "C: %1.2e F" % (1 / (math.log(2) * args.f * (args.r1 + 2 * args.r2)))
	except:
	    print error_msg

    elif args.r1 == None:
	try:
	    print "R1: %d Ohms" % (1 / (math.log(2) * args.f * args.c) - 2 * args.r2)
	except:
	    print error_msg

    elif args.r2 == None:
        try:
	    print "R2: %d Ohms" % (1 / (2 * math.log(2) * args.f * args.c) - args.r1 / 2)
	except:
	    print error_msg

elif args.monostable:
    if args.t == None:
	try:
	    print "T: %1.2e seconds" % (args.r * args.c * math.log(3))
	except:
	    print error_msg

    elif args.c == None:
	try:
	    print "C: %1.2e F" % (args.t / (args.r * math.log(3)))
	except:
	    print error_msg

    elif args.r == None:
	try:
	    print "R: %d Ohms" % (args.t / (args.c * math.log(3)))
	except:
	    print erro_msg
