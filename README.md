# README #

This repository contains python scripts I wrote for myself to deal with simple yet frequent electrical engineering problems, while also learning python. It continues to be updated as I work on new things.

### Scripts ###

* 555Timer.py - Calculate any parameter for typical monostable and astable configurations.
* rcfilter.py - Calculate any parameter for a basic RC filter.
* resistor.py - Convert between color codes and numerical values.
* rled.py - Calculate appropriate resistor value for given LED and source voltage parameters.