#!/usr/bin/python
#
# Written by James Gregoire, April 2013
#
# This script takes 3 or 4 color arguments and outputs the resistor value,
# or takes a resister value and outputs the color code.
#
import sys
import math

n_args = len(sys.argv) - 1
color_list = (("black",0), ("brown",1), ("red",2), ("orange",3), ("yellow",4), ("green",5), ("blue",6), ("violet",7), ("purple",7), ("grey",8), ("gray",8), ("white",9))

usage = "Usage: Enter a valid resistor value (e.g. 10000) or color code (brown black orange)"

if n_args == 1:

	# User hopefully entered a numeric value
	try:
		value = int(sys.argv[1]) # Will break if non-numeric

		# Calculate power
		power = int(math.log10(value))

		# Resistors aren't in scientific notation, though. (A.B * 10^C)
		# Instead the formula is (10*A + B) * 10^C, so C is just less one.
		c = power - 1

		if c > 9:
			print "Resistors this large cannot be represented by color bars."
			exit()

		if c < 0:
			print "Resistors this small cannot be represented by color bars."
			exit()

		# Reduce value to two sig figs
		sig = value / pow(10, c)

		# Find A
		a = int(sig / 10)

		# Find B
		b = sig - a * 10

		# Calculate resistance having rounded down from input
		resistance = (10 * a + b) * pow(10, c)

		# Find colors
		for color in color_list:
			if color[1] == a:
				a = color[0]

			if color[1] == b:
				b = color[0]

			if color[1] == c:
				c = color[0]

		# Output result
		print "%d Ohms = %s %s %s" % (resistance, a, b, c)

	except:
		print usage

elif n_args == 3:

	try:
		for arg in sys.argv:
			arg = arg.strip().lower()
	except:
		print usage

	# Make sure user entered valid colors
	good_args = 0.

	for color in color_list:
		if color[0] == sys.argv[1]:
			val1 = color[1]
			good_args = good_args + 1

		if color[0] == sys.argv[2]:
			val2 = color[1]
			good_args = good_args + 1

		if color[0] == sys.argv[3]:
			val3 = color[1]
			good_args = good_args + 1

	# Calculate value		
	if good_args == 3:
		value = (10*val1 + val2) * pow(10, val3)
		print "Resistor value: %d Ohms" % (value)
				
	else:
		print usage

elif n_args == 4:

	# Make sure user entered valid colors
	good_args = 0

	for color in color_list:
		if color[0] == sys.argv[1]:
			val1 = color[1]
			good_args = good_args + 1

		if color[0] == sys.argv[2]:
			val2 = color[1]
			good_args = good_args + 1

		if color[0] == sys.argv[3]:
			val3 = color[1]
			good_args = good_args + 1

		if color[0] == sys.argv[4]:
			val4 = color[1]
			good_args = good_args + 1

	# Calculate value
	if good_args == 4:
		value = (100*val1 + 10*val2 + val3) * pow(10, val4)
		print "Resistor value: %f Ohms" % (value)
	else:
		print usage
	

else:
	
	print usage
