#!/usr/bin/python
#
# Written by James Gregoire, April 2013
#
# This script takes any two values of resistance, capacitance, and frequency, and
# calculates the third for an RC filter, assuming ideal components. Always make sure
# to research how your specific components behave beyond 100 kHz.
#
# Usage: "python resistor.py [-r 10k] [-c 220n] [-f 20k]" (any two of r, c, and f)
#

import sys
import getopt
import math
import decimal

# This function translates something like "220n" into an actual number
def getVal(value, suffixes):

	# Pull off last character
	suffix = value[len(str(value)) - 1] # I hate dynamic typing sometimes


	# Is it a number, or a proper suffix?
	if suffix.isdigit() == False:

		# Determine multiplier
		found = False
		for letter in suffixes:
			if letter[0] == suffix:
				power = letter[1]
				found = True

		if found == True:
			try:
				val = float(value[:len(value) -1])
				return val * float(pow(10, power))
			except:
				raise StandardError("Invalid input: %s" % (value))
				return None

		else:
			raise StandardError("Bad suffix: %s" % (suffix))
			return None

	else:
		return float(value)

# This function turns a float into a string formatted as engineering notation
def engify(value):
	return decimal.Decimal(float(value)).normalize().to_eng_string()

# And on with the show!


usage = "Usage: [-r 10k] [-c 2.2u] [-f 100k] (Any two returns the third)"

# Grab arguments, make sure they're good
try:
	optlist, args = getopt.getopt(sys.argv[1:], "r:c:f:")
	good_call = True

except:
	print usage
	good_call = False

if good_call == True and len(optlist) == 2:

	# Is that even how you spell it? Suffices? Whatever.
	suffixes = (("G", 9), ("M", 6), ("k", 3), ("m", -3), ("u", -6), ("n", -9), ("p", -12))

	try:
		# Pull out numeric values
		for option in optlist:
			if option[0] == "-c":
				c = getVal(option[1], suffixes)

			if option[0] == "-r":
				r = getVal(option[1], suffixes)

			if option[0] == "-f":
				f = getVal(option[1], suffixes)

	except StandardError as e:
		print str(e)
		sys.exit()


	pi = 3.1415926

	# What are we calculating?
	if optlist[0][0] != "-c" and optlist[1][0] != "-c":

		c = 1 / (2 * pi * r * f)
		print "Capacitance: %s F" % (engify(c))

	elif optlist[0][0] != "-r" and optlist[1][0] != "-r":

		r = 1 / (2 * pi * c * f)
		print "Resistance: %s Ohms" % (engify(r))

	else:

		f = 1 / (2 * pi * r * c)
		print "Frequency: %s Hz" % (engify(f))


elif good_call == True:
	print usage

