#!/usr/bin/python
#
# This script is used to calculate the necessary resistor to be paired with an LED.
#
# USAGE:
#
# ./rled.py -i 30 -v 2.1 -V 5.0
# ./rled.py --iled=30 --vled=2.1 --vin=5.0
#
# PARAMETERS:
#
#    -i/iled LED forward current
#    -v/vled LED forward voltage
#    -V/vin  Source voltage
#

import argparse

# Parse arguments
parser = argparse.ArgumentParser(description='Calculate appropriate resistor value for given LED and source voltage')

parser.add_argument("-i", "--iled", type=float, required=True, metavar="MILLIAMPS", help="Forward current of LED in milliamps (mA).")
parser.add_argument("-v", "--vled", type=float, required=True, metavar="VOLTS", help="Forward voltage of LED in Volts (V).")
parser.add_argument("-V", "--vin",  type=float, required=True, metavar="VOLTS", help="Source voltage in Volts (V).")

args = parser.parse_args()

try: # I don't trust you, user.
    rled = args.vled / (args.iled / 1000)                   # Ohm's Law
    print "%d Ohms" % ( rled * args.vin / args.vled - rled) # Voltage divider law
except:
    print "Invalid arguments. Type './rled.py -h' for more info."
